# Install

`stow -t ~ -v base`

# Uninstall

`stow -t ~ -vD base`

# ZSH

For zsh to use the stowed config, it needs `~/.zshenv` to have:
```
ZDOTDIR=$HOME/.config/zsh/
```