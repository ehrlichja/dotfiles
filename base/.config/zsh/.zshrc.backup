# style

autoload -U colors && colors
PROMPT='%m %F{67}%~ %F{255}> '

# git integration

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{240}(%b) %r%f'
zstyle ':vcs_info:*' enable git

# zsh options
setopt APPEND_HISTORY
setopt AUTO_CD
setopt CORRECT
setopt CORRECT_ALL
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY
setopt INC_APPEND_HISTORY
setopt NO_CASE_GLOB
setopt SHARE_HISTORY
HISTSIZE=10000
SAVEHIST=10000
HISTFILE="$HOME/.zsh_history"

# basic auto/tab complete:

zstyle ':completion:*' list-suffixes zstyle ':completion:*' expand prefix suffix 
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# use vim keys in tab complete menu:

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# change cursor shape for different vi modes.

function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line


# path

## macos

[ -f "$HOME/.google-cloud-sdk/path.zsh.inc" ] && source $HOME/.google-cloud-sdk/path.zsh.inc

[ -f "$HOME/.google-cloud-sdk/completion.zsh.inc" ] && source $HOME/.google-cloud-sdk/completion.zsh.inc

[ -f "/usr/local/etc/profile.d/z.sh" ] && source /usr/local/etc/profile.d/z.sh

[ -d "$HOME/.google-cloud-sdk/bin/" ] && export PATH=$HOME/.google-cloud-sdk/bin/:$PATH

[ -d "$HOME/Library/Android/" ] && export PATH=\
~/Library/Android/sdk/platform-tools:\
~/Library/Android/sdk/emulator:\
~/Library/Android/sdk/tools/bin:$PATH

[ -d "$HOME/Library/Application Support/Coursier/bin" ] && export PATH=\
~/Library/Application\ Support/Coursier/bin:$PATH

[ -d "/usr/local/texlive/2020/bin/x86_64-darwin/pdflatex" ] && export PATH=\
/usr/local/texlive/2020/bin/x86_64-darwin/pdflatex:$PATH

## freebsd

[ -f "/usr/local/share/z/z.sh" ] && source /usr/local/share/z/z.sh

export PATH=\
/usr/local/bin:\
/usr/local/sbin:\
~/bin:\
~/.local/bin:\
$PATH

export MANPATH=\
/usr/share/man:\
/usr/local/share/man:\
/usr/X11/share/man:\
$MANPATH

# Load aliases and shortcuts, and completions

[ -f "$HOME/.config/zsh/shortcutrc" ] && source "$HOME/.config/zsh/shortcutrc"
[ -f "$HOME/.config/zsh/aliasrc" ] && source "$HOME/.config/zsh/aliasrc"
[ -f "$HOME/.fzf.zsh" ] && source ~/.fzf.zsh
[ -f "$HOME/.zshrc" ] && source $HOME/.zshrc
[ -f "$HOME/.zshextra" ] && source ~/.zshextra
[ -f /usr/local/bin/aws_zsh_completer.sh ] && source /usr/local/bin/aws_zsh_completer.sh
[ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
if command -v doctl &> /dev/null
then
  source <(doctl completion zsh) || true
fi

# custom env variables

export BROWSER="open"
export EDITOR="nvim"
export FZF_CTRL_R_OPTS="--preview-window=hidden"
export FZF_CTRL_T_COMMAND="rg --hidden --files -g '!.git'"
export FZF_CTRL_T_OPTS="--height 60% --preview 'bat --color always {}'"
export FZF_DEFAULT_COMMAND="rg --hidden --files -g '!.git'"
export FZF_DEFAULT_OPTS="--height 40% --preview 'bat --color always {}'"
export GIT_EDITOR="nvim"
export GPG_TTY=$(tty)

# start keychain

if command -v keychain &> /dev/null
then
  eval $(keychain --quiet --eval aehrlich)
fi


# nnn cd on quit
nn ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, either remove the "export" as in:
    #    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    #    (or, to a custom path: NNN_TMPFILE=/tmp/.lastd)
    # or, export NNN_TMPFILE after nnn invocation
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

eval "$(/opt/homebrew/bin/brew shellenv)"


#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
