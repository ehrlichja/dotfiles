" Plugins
" Plugins will be downloaded under the specified directory.
call plug#begin(stdpath('data') . '/plugged')
Plug 'airblade/vim-gitgutter'
Plug 'andymass/vim-matchup'
Plug 'arcticicestudio/nord-vim'
Plug 'chrisbra/csv.vim'
Plug 'derekwyatt/vim-scala'
Plug 'elzr/vim-json'
Plug 'hrsh7th/vim-vsnip'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'othree/xml.vim'
Plug 'qpkorr/vim-bufkill'
Plug 'scrooloose/nerdtree'
Plug 'sukima/xmledit'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

" Basic Settings
set autoindent
set background=dark
set cmdheight=2
set cursorline
set encoding=utf-8
set expandtab
set hidden
set incsearch
set mouse=a
set nobackup
set nowritebackup
set number
set number relativenumber
set scrolloff=30
set shiftwidth=2
set smartindent
set shortmess-=F
set spellfile=$HOME/.config/nvim/en.utf-8.add
set splitbelow splitright
set tabstop=2
set timeoutlen=500
set updatetime=300
set wildmode=longest,list,full
set wrap linebreak nolist
set autoread


" Key combos
:command M lua require("metals").initialize_or_attach(metals_config)

nnoremap <SPACE> <Nop>
let mapleader = " "
nnoremap <leader>5 :vsplit<cr>
nnoremap <leader>c :close<cr>
nnoremap <leader>d :BD<cr>
nnoremap <leader>f viw"+y:FZF<cr>
nnoremap <leader>g :Git blame<cr>
nnoremap <silent>ma <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <silent>mD <cmd>lua vim.diagnostic.setqflist() <CR>
nnoremap <silent>md <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent>mi <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent>mk <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent>mn <cmd>lua vim.lsp.buf.rename()<CR>
nnoremap <silent>mr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent>ms <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <leader>l ipprint.pprintln()<ESC>i
nnoremap <leader>m :Glow<cr>
nnoremap <leader>n :bn<cr>
nnoremap <leader>o :only<cr>
nnoremap <leader>p :bp<cr>
nnoremap <leader>q :qa<cr>
nnoremap <leader>r viw"+y:Rg<cr>
nnoremap <leader>s :TagbarToggle<cr>
nnoremap <leader>t :NERDTreeFind<cr>
nnoremap <leader>v "+p 
nnoremap <leader>w :w<cr>
nnoremap <leader>x :xa<cr>
nnoremap <leader>z $zf%<cr>

" Style
syntax enable
let g:airline_theme='simple'
highlight Pmenu ctermbg=grey gui=bold
 
autocmd FileType json setlocal foldmethod=syntax

filetype plugin indent on

" Configs for specific plugins
" Config for vim-airline
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
  " Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'
" Config for vim-scala
let g:scala_scaladoc_indent = 1

lua require('init')
